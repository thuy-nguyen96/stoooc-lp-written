import { useEffect } from 'react'
import { AppProps } from 'next/app'
import Font from '../components/Font'
import '../styles/index.css'

export default function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    process.env.NODE_ENV !== 'production' && console.info('Loading fonts...')
    Font()
  }, [])

  return <Component {...pageProps} />
}

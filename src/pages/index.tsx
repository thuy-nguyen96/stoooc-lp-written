import Head from 'next/head'
import ScrollMenu from 'react-horizontal-scrolling-menu'
import classnames from 'classnames'

const SLOGAN = `あなたのアイデアが\n誰かを楽しませるかもしれない、誰かを前向きにさせるかもしれない\n\nあなたのアイデアが、\n誰かを救うかもしれない、誰かを幸せにするかもしれない\n\n世界を変える、あなたのアイデアとともに\n\n`

const MEMEBERS = [
  {
    name: 'Natsumi Adachi',
    title: 'Director',
    // picture: '/members/NatsumiAdachi.jpg',
  },
  {
    name: 'Edward Lee',
    title: 'Technical Director',
    // picture: '/members/EdwardLee.jpg',
  },
  {
    name: 'Ryosuke Taketazu',
    title: 'Design Director',
    // picture: '/members/RyosukeTaketazu.jpg',
  },
  {
    name: 'Akiko Gushiken',
    title: 'Director',
    // picture: '/members/AkikoGushiken.jpg',
  },
  {
    name: 'Jiro Ishii',
    title: 'Director',
    picture: '/members/JiroIshii.jpg',
  },
  {
    name: 'Hikari Yamamoto',
    title: 'Technical Director',
    picture: '/members/HikariYamamoto.jpg',
  },
]

const CASES = [
  {
    src: '/logos/nailie.svg',
    href: 'http://nailie.jp',
    alt: 'nailie',
  },
  {
    src: '/logos/soraemon.svg',
    href: 'https://soraemon.com',
    alt: 'soraemon',
  },
  {
    src: '/logos/hogugu.svg',
    href: 'https://hogugu.com',
    alt: 'hogugu',
  },
  {
    src: '/logos/shikomel.svg',
    href: 'https://shikomel.com',
    alt: 'shikomel',
  },
  {
    src: '/logos/shachomeshi.svg',
    href: 'https://shachomeshi.com',
    alt: 'shachomeshi',
  },
  {
    src: '/logos/noteE.svg',
    href: null,
    alt: 'noteE',
  },
  {
    src: '/logos/ekuipp.svg',
    href: 'https://ekuipp.com',
    alt: 'ekuipp',
  },
  {
    src: '/logos/tanomel.svg',
    href: 'https://www.tanomel.com',
    alt: 'tanomel',
  },
]

const renderMembers = (list, clsName) =>
  list.map((m) => {
    const { name, title, picture } = m
    return (
      <div className={clsName} key={`key__${name}`}>
        <div className="sm:w-38 flex flex-row items-center">
          <div className="self-start">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="14"
              height="14"
              viewBox="0 0 14 14"
            >
              <path
                fill="#AFAFAF"
                fillRule="evenodd"
                d="M14 .636H.637V14H0L.001.636H0V0h14v.636z"
              />
            </svg>
          </div>
          <div className="avatar">
            <img
              src={
                picture ||
                'https://api.adorable.io/avatars/285/abott@adorable.png'
              }
              alt={`${name}`}
            />
          </div>
          <div className="self-end">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="14"
              height="14"
              viewBox="0 0 14 14"
            >
              <path
                fill="#AFAFAF"
                fillRule="evenodd"
                d="M0 13.364h13.363V0H14l-.001 13.364H14V14H0v-.636z"
              />
            </svg>
          </div>
        </div>
        <div className="block self-center">
          <div className="w-40 text-lg font-medium leading-8 text-dark-gray truncate">
            {name}
          </div>
          <div className="font-medium leading-8 text-gray">{title}</div>
        </div>
      </div>
    )
  })

export const Home = (): JSX.Element => {
  return (
    <div className="min-w-full min-h-screen flex flex-col">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="text-base text-left lg:text-lg lg:mx-auto lg:flex lg:flex-col lg:justify-center lg:items-center">
          {/* Start of section banner + slogan */}
          <div className="container px-4 lg:w-not-phone">
            <img
              className="w-40 m-b-2 pt-11 pb-4 lg:w-logo-not-phone"
              src="/logo-banner.png"
              alt="Stooc"
            />
            <img className="w-22 py-4" src="/slashes.png" alt="slashes" />
            <h1 className="text-4xl lg:text-6xl leading-8 lg:leading-16 text-left text-primary font-black pb-9">
              We embrace people who change the world
            </h1>
          </div>
          <div className="container px-4 banner-container lg:min-w-full">
            <p className="text-lg text-left leading-8 text-primary bg-white font-medium whitespace-pre-wrap lg:hidden">
              {SLOGAN}
            </p>
            <div className="banner-box">
              <p
                className="bg-white text-lg leading-8 text-left text-primary font-medium py-5 pr-10 lg:hidden"
                style={{ width: 'fit-content' }}
              >
                Start to change. Stoooc.
              </p>
              <p
                className="text-lg text-left hidden lg:block leading-8 text-primary bg-white font-medium whitespace-pre-wrap p-8 pl-0"
                style={{ width: 'fit-content' }}
              >
                {`${SLOGAN}Start to change. Stoooc.`}
              </p>
            </div>
          </div>
          {/* Start of section about us */}
          <div className="w-full px-4 py-18 lg:py-not-phone bg-black">
            <div className="container flex flex-col lg:w-not-phone lg:mx-auto">
              <div className="max-w-sm mb-8">
                <h2 className="inline-block text-5xl leading-14 text-left align-bottom text-white font-black">
                  About us
                </h2>
                <span
                  className="inline-block px-1"
                  style={{ paddingLeft: '3px' }}
                >
                  <img src="./ellipse.svg" alt="Header ellipse" />
                </span>
              </div>
              <div className="inline-block">
                <p className="text-lg text-left text-white font-medium whitespace-pre-wrap lg:pr-19">
                  {`Stooocは世の中のさまざまな課題に技術とアイデアで取り組むクリエイティブカンパニーです。\n\n私たちはプロジェクトの初期段階からサービスの構築・運用まで、全ての過程で最適な技術とアイデアを提案し、新しいサービスの実現をサポートしています。\n\nStooocは目に見えないアイデアを形にし、あなたのビジネスを成功に導きます。`}
                </p>
              </div>
            </div>
          </div>
          {/* Start of section team */}
          <div className="min-w-full px-4 py-18 bg-white">
            <div className="max-w-sm mb-8">
              <h2 className="inline-block text-5xl leading-14 text-left align-bottom text-black font-black">
                Our team
              </h2>
              <span
                className="inline-block px-1"
                style={{ paddingLeft: '3px' }}
              >
                <img src="./ellipse.svg" alt="Header ellipse" />
              </span>
            </div>
            <div className="block lg:hidden">
              <ScrollMenu
                data={renderMembers(MEMEBERS, 'scroll-item-wrapper ')}
                arrowLeft={
                  <div className="absolute left-0 w-8 h-8 pl-4 m-auto z-10 cursor-pointer">
                    <svg
                      width="32"
                      height="32"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g fill="none" fillRule="evenodd">
                        <circle
                          stroke="#ECECEC"
                          fillOpacity=".7"
                          fill="#FFF"
                          cx="16"
                          cy="16"
                          r="15.5"
                        />
                        <path
                          stroke="#AFAFAF"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M17 11l-5 5 5 5"
                        />
                      </g>
                    </svg>
                  </div>
                }
                arrowRight={
                  <div className="absolute right-0 w-8 h-8 pr-12 m-auto z-10 cursor-pointer">
                    <svg
                      width="32"
                      height="32"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g
                        transform="rotate(-180 16 16)"
                        fill="none"
                        fillRule="evenodd"
                      >
                        <circle
                          stroke="#ECECEC"
                          fillOpacity=".7"
                          fill="#FFF"
                          cx="16"
                          cy="16"
                          r="15.5"
                        />
                        <path
                          stroke="#AFAFAF"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M17 11l-5 5 5 5"
                        />
                      </g>
                    </svg>
                  </div>
                }
                translate={-8}
                wheel={false}
              />
            </div>
          </div>
          {/* Start of section case */}
          <div className="min-w-full px-4 py-18 bg-white">
            <div className="max-w-sm mb-8">
              <h2 className="inline-block text-5xl leading-14 text-left align-bottom text-primary font-black">
                Case
              </h2>
              <span
                className="inline-block px-1"
                style={{ paddingLeft: '3px' }}
              >
                <img src="./ellipse.svg" alt="Header ellipse" />
              </span>
            </div>

            <div className="inline-block">
              <p className="text-xl text-left leading-8 text-primary font-black whitespace-pre-wrap pb-6">
                {`Stooocはビジネスの形にとらわれず、\n様々なサービスを展開しています。`}
              </p>
            </div>

            <div className="grid grid-flow-row grid-cols-3 lg:grid-cols-4 grid-rows-3 lg:grid-rows-2 gap-0 place-content-stretch place-items-auto justify-items-stretch border-b border-table divide-y divide-table">
              {CASES.map((c, index) => {
                const clsName = classnames(
                  'flex items-center min-h-table-items max-h-table-items',
                  { 'border-t border-table': index === 0 }
                )
                let overrideStyles = {}
                if (index === 5) {
                  overrideStyles = { borderBottomWidth: '1px' }
                }

                return (
                  <div
                    className={clsName}
                    key={c.alt}
                    style={{ ...overrideStyles }}
                  >
                    <a href={c.href} target="_blank" rel="noopener noreferrer">
                      <img src={c.src} alt={c.alt} />
                    </a>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

export default Home

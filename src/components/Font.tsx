const FontFaceObserver = require('fontfaceobserver')

const Fonts = () => {
  const notosansjp = new FontFaceObserver('NotoSansJP')

  notosansjp
    .load()
    .then(() => {
      process.env.NODE_ENV !== 'production' &&
        console.info('Load font successfully')
    })
    .catch((e) => console.error(e))
}

export default Fonts

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
  },
  purge: [
    './src/components/**/*.{js,ts,jsx,tsx}',
    './src/pages/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['NotoSansJP', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        primary: '#2a292c',
        secondary: '#939393',
        footer: '#0f0f0f',
        additional: '#ea4435',
      },
      width: {
        'logo-not-phone': '18.625rem',
        'not-phone': '48.875rem',
      },
      height: {
        'not-phone': '72.375rem',
      },
      minHeight: {
        'table-items': '3.125rem',
      },
      maxHeight: {
        'table-items': '6.255rem',
      },
      padding: {
        9: '2.25rem',
        11: '2.875rem',
        14: '3.5rem',
        18: '4.5rem',
        19: '4.875rem',
        'not-phone': '8.75rem',
      },
      spacing: {
        38: '8.75rem',
      },
      fontSize: {
        xs: '14px',
        base: '16px',
        '4xl': '2rem',
      },
      lineHeight: {
        12: '3rem',
        14: '3.5rem',
        16: '4rem',
      },
      borderColor: {
        table: '#f3f3f3',
      },
      divideColor: {
        table: '#f3f3f3',
      },
    },
  },
  variants: {},
  plugins: [],
}
